class DoorsController < ActionController::API

  before_action :validate_residence_id
  before_action :validate_resident_id

  # GET sugar/api/residence/<residence_id>/resident/<resident_id>/doors
  def index
    binding.pry
    Doors.forResident(params[:resident_id])
    # TODO
    render json: { }
  end

  private

  def validate_residence_id
    # return unless Residence.valid?(params[:residence_id])
  end

  def validate_resident_id
    # return unless Resident.valid?(params[:resident_id])
  end
end
