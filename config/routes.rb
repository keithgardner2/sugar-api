Rails.application.routes.draw do

  # List Doors for given resident
  get 'sugar/api/residence/:residence_id/resident/:resident_id/doors', to: 'doors#index'

end
